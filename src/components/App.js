import React from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import Inventory from './Inventory';
import Order from './Order';
import sampleFishes from '../sample-fishes';
import Fish from './Fish';
import base from '../base';

class App extends React.Component {
  state = {
    fishes: {},
    order: {}
  };

  static propTypes = {
    match: PropTypes.object
  }

  componentDidMount() {
    const { params } = this.props.match;
    // Reinstate localStorage
    const localStorageRef = localStorage.getItem(params.storeId);
    if(localStorageRef) {
      this.setState({ order: JSON.parse(localStorageRef) });
    }
    this.ref = base.syncState(`${params.storeId}/fishes`, {
      context: this,
      state: 'fishes'
    });
  }

  componentDidUpdate() {
    console.log(this.state.order);
    localStorage.setItem(this.props.match.params.storeId, JSON.stringify(this.state.order));
  }

  componentWillUnmount() {
    base.removeBindig(this.ref);
  }

  addFish = fish => {
    const fishes = { ...this.state.fishes };
    fishes[`fish${Date.now()}`] = fish;
    this.setState({
      fishes
    });
  };

  updateFish = (key, updatedFish) => {
    // take copy of current state
    const fishes = {...this.state.fishes};
    // update the state
    fishes[key] = updatedFish;
    // set that to state
    this.setState({ fishes });
  }

  loadSampleFishes = () => {
    this.setState({ fishes: sampleFishes });
  };

  deleteFish = (key) => {
    // take a copy of state
    const fishes = { ...this.state.fishes };
    // set the fish's object to null - because of Firebase
    fishes[key] = null;
    // update state
    this.setState({ fishes });
  }

  addToOrder = (key) => {
    // take a copy of state
    const order = { ...this.state.order };
    // add to the order, or update the number of item
    order[key] = order[key] + 1 || 1;
    // call setState to update or state object
    this.setState({ order });
  }

  removeFromOrder = (key) => {
    // take a copy of state
    const order = { ...this.state.order };
    // remove from order
    delete order[key];
    // call setState to update or state object
    this.setState({ order });
  }

  render () {
    return (
        <div className="catch-of-the-day">
            <div className="menu">
                <Header tagline="Fresh Seafood Market" />
                <ul className="fishes">
                  {Object.keys(this.state.fishes).map(key => 
                    <Fish 
                    key={key} 
                    index={key} 
                    details={this.state.fishes[key]} 
                    addToOrder={this.addToOrder} 
                    />
                  )}
                </ul>
            </div>
            <Order 
              fishes={this.state.fishes}
              order={this.state.order}
              removeFromOrder={this.removeFromOrder}
            />
            <Inventory 
              addFish={this.addFish} 
              updateFish={this.updateFish} 
              deleteFish={this.deleteFish} 
              loadSampleFishes={this.loadSampleFishes}
              fishes={this.state.fishes}
              storeId={this.props.match.params.storeId}
            />
        </div>
    );
  }
}

export default App;