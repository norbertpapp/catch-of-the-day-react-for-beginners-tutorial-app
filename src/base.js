import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyBbbuWD1kJoS6rND5SIk7x1qsmjQkkGNMY",
  authDomain: "catch-of-the-day-norbeepapp.firebaseapp.com",
  databaseURL: "https://catch-of-the-day-norbeepapp.firebaseio.com",
  appId: "1:58919086435:web:9293a773c38442a4f28b56"
});

const base = Rebase.createClass(firebaseApp.database());

// This is a named export
export { firebaseApp };

// This is a default export
export default base;